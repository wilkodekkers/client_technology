const {src, dest} = require('gulp');
const gulpSass = require('gulp-sass');
const concat = require('gulp-concat');

const sass = (backEndPath, filesSass) => {
  return () => {
    return src(filesSass)
      .pipe(gulpSass().on('error', gulpSass.logError))
      .pipe(dest('./dist/sass'))
      .pipe(concat('style.min.css'))
      .pipe(dest('./dist/css'))
      .pipe(dest(backEndPath + '/css'));
  };
};

exports.sass = sass;
