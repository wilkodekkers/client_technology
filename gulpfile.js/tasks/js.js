const {src, dest} = require('gulp');
const order = require('gulp-order');
const concat = require('gulp-concat');
const babel = require('rollup-plugin-babel');
const minify = require('gulp-minify');
const rollup = require('gulp-better-rollup');
const resolve = require('rollup-plugin-node-resolve');

const fn = (backendPath, fileJs, fileJsOrder) => {
  return () => {
    return src(fileJs)
      .pipe(order(fileJsOrder, {base: './'}))
      .pipe(rollup({
        plugins: [
          resolve(),
          babel({
            presets: [['@babel/preset-env', {
              modules: false,
              targets: {esmodules: true},
            }]],
            plugins: [
              ['@babel/plugin-proposal-decorators', {'decoratorsBeforeExport': true}],
              ['@babel/plugin-proposal-class-properties'],
            ],
          }),
        ],
      }, 'umd'))
      .pipe(concat('bundle.js'))
      .pipe(minify({noSource: true, ext: {min: '.min.js'}}))
      .pipe(dest('./dist/js'))
      .pipe(dest(backendPath + '/js'));
  };
};

exports.js = fn;
