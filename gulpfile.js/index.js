const config = require('./config');
const {watch, series} = require('gulp');
const js = require('./tasks/js').js(
  config.localServerProjectPath,
  config.files.js,
  config.fileJsOrder);
const sass = require('./tasks/sass').sass(
  config.localServerProjectPath,
  config.files.sass
);

js.displayName = 'js';
sass.displayName = 'sass';

const hello = (done) => {
  console.log(`Groeten van ${config.voornaam}!`);
  done();
};

const watchFiles = () => {
  watch(['src/js/*.js'], series(js));
  watch(['src/css/*.scss'], series(sass));
};

exports.default = hello;
exports.watch = watchFiles;
exports.sass = sass;
exports.js = js;
