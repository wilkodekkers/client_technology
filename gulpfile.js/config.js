module.exports = {
  localServerProjectPath: 'C:/Users/wilko/GitHub/reversi/ReversiApp/wwwroot',
  fileJsOrder: ['game.js', 'app.js'],
  files: {
    js: [
      'src/js/**/*.js',
      'src/js/*.js',
      'node_modules/@babel/polyfill/dist/polyfill.js',
    ],
    sass: [
      'src/css/**/*.scss',
      'src/css/*.scss',
    ],
  },
};
