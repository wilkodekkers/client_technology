describe('A suite', function() {
  it('contains spec with an expectation', function() {
    expect(true).toBe(true);
  });
});

describe('Application initialisation', function() {
  it('Method init returns true', function() {
    const result = myApp.init();
    expect(result).toBe(true);
  });
});

describe('feedback widget', function() {
  it('widget hides element', function() {
    const selector = document.getElementById('feedback-success');
    const widget = new FeedbackWidget('feedback-success');

    widget.hide();

    expect(selector).toBeDefined();
    expect(selector.style.display).toBe('none');
  });
});

describe('game test', function() {
  it('game is global available', function() {
    expect(Game).toBeDefined();
  });

  it('game has init method', function() {
    const method = Game.init;
    expect(method).toBeDefined();
  });

  it('game reverse has init method', function() {
    const method = Game.Reversi.init;
    expect(method).toBeDefined();
  });

  it('game data has init method', function() {
    const method = Game.Data.init;
    expect(method).toBeDefined();
  });

  it('game model has init method', function() {
    const method = Game.Model.init;
    expect(method).toBeDefined();
  });
});
