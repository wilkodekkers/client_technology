import { customElement, property, html, css, LitElement } from 'lit-element';
import '@granite-elements/granite-spinner';

@customElement('reversi-game')
export class ReversiGame extends LitElement {
  @property({ type: Game }) game;

  static get styles() {
    return css`
      #card {
        display: inline-block;
        padding: 2px;
        background-color: #7be3c2;
        border-radius: 5px;
      }
      
      .card__container {
        display: flex;
      }
      
      .card__cell {
        width: 100px;
        height: 100px;
        background-color: #a7f9df;
        border: 2px solid #7be3c2;
        display: flex;
        justify-content: center;
        align-items: center;
      }
      
      .card__cell:hover {
        background-color: #dafff3;
        cursor: pointer;
      }
      
      .card__tile {
          width: 90px;
          height: 90px;
          border-radius: 20px;
          background-color: #3dbdee;
          border: 3px solid #24aadd;
        }
       
      .tile__black {
        background-color: #ff68a8;
        border: 3px solid #ed4d90;
      }
    `;
  }

  constructor() {
    super();

    this.game = Game;
    this.game.init(() => console.log('init game'));

    setInterval(() => this.requestUpdate(), 1000 / 60);
  }

  placeTile(row, col) {
    this.game.Reversi.showFiche(row, col)
  }

  render() {
    const { playerTurn, board, over, winning } = this.game.gameState;

    if (board.length === 0) {
      return html`<granite-spinner 
                    color="#3dbdee" 
                    line-width="1em" 
                    active></granite-spinner>`;
    }

    return html`
        <link rel="stylesheet" href="/lib/bootstrap/dist/css/bootstrap.min.css" />
        <div class="row">
          <div class="col-4">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">${playerTurn === 1 ? 'Blue' : 'Purple'} Turn</h5>
                <h5 class="card-title">${winning === 1 ? 'Blue' : 'Purple'} is Winning</h5>
                <h5 class="card-title">${over ? 'Game Over' : ''}</h5>
              </div>
            </div>
          </div>
          <div class="col-8">
            <div id="card">
             ${board.map((row, ri) => html`<div class="card__container">
                ${row.map((col, ci) => html`<div class="card__cell" @click="${() => this.placeTile(ri, ci)}">
                    ${col === 1 ? html`<div class="card__tile"></div>` : html``}
                    ${col === 2 ? html`<div class="card__tile tile__black"></div>` : html``}
                </div>`)}
             </div>`)}
            </div>
          </div>
        </div>
    `;
  }
}
