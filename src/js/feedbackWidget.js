/**
 * Feedback widget
 */
export class FeedbackWidget {
    /**
     * constructor
     * @param {*} elementId id from element
     */
    constructor(elementId) {
        this._elementId = elementId;
    }

    /**
     * show message box
     * @param {*} message string with message
     * @param {*} type string success or other
     */
    show(message, type) {
        const container = document.getElementById(this._elementId);
        const paragraph = container.childNodes[0];

        container.css('display', 'block');
        paragraph.text(message);

        if (type === 'success') {
            container.classList.add('alert-success');
        } else {
            container.classList.add('alert-danger');
        }

        this.log({message, type});
        this.history();
    }

    /**
     * hide element
     */
    hide() {
        document.getElementById(this._elementId).style.display = 'none';
    }

    /**
     * add message to localstorage
     * @param {*} message string message
     */
    log(message) {
        const storage = JSON.parse(localStorage.getItem('feedback_widget'));

        while (storage.length >= 10) storage.shift();

        storage.push(message);
        localStorage.setItem('feedback_widget', JSON.stringify(storage));
    }

    /**
     * remove item from localstorage
     */
    removeLog() {
        localStorage.removeItem('feedback_widget');
    }

    /**
     * show history
     */
    history() {
        const storage = JSON.parse(localStorage.getItem('feedback_widget'));

        storage.map((x) => console.log(`${x.type} - ${x.message}`));
    }
}
