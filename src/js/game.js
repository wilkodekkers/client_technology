const apiUrl = 'url/super/duper/game';

window.Game = (function (url) {
  const configMap = {
    apiUrl: url,
  };

  const stateMap = {
    playerTurn: 0,
    board: [],
    winning: 0,
    over: false,
  };

  /**
   * initialize game
   * @param {*} callback method
   */
  function privateInit(callback) {
    console.log(configMap.apiUrl);
    _getCurrentGameState();
    callback();
  }

  /**
   *
   * @private
   */
  function _getCurrentGameState() {
    setInterval(() => {
      Game.Model.getGameState()
      .then((res) => {
        stateMap.playerTurn = res.AandeBeurt;
        stateMap.board = res.Bord;
      })
      .catch((err) => console.error(err))

      Game.Model.getOver()
      .then((res) => stateMap.over = res)
      .catch((err) => console.error(err));

      Game.Model.getWinning()
      .then((res) => stateMap.winning = res)
      .catch((err) => console.error(err));

    }, 1000);
  }

  return {
    init: privateInit,
    gameState: stateMap
  };
})(apiUrl);

Game.Reversi = (function () {
  const configMap = {};

  /**
   * initialize reversi
   */
  function privateInit() {
  }

  /**
   * add fiche to the screen
   * @param {int} x position
   * @param {int} y position
   */
  function showFiche(x, y) {
    fetch(`https://localhost:44330/api/Spel/${window.token}/place?playerToken=${window.playerToken}&x=${x}&y=${y}`, { method: 'post' });
  }

  return {
    init: privateInit,
    showFiche,
  };
})();

Game.Data = (function () {
  const configMap = {
    apiKey: '9d6332d975e18eb578ebaf5a33eda23d',
    mock: [
      {
        url: 'api/Spel/Beurt',
        data: 0,
      },
    ],
  };

  const stateMap = {
    environment: 'production',
  };

  /**
   * initialize data
   * @param {string} environment
   */
  function privateInit(environment) {
    if (environment !== 'development' && environment !== 'production') {
      throw new Error('invalid environment type');
    }

    stateMap.environment = environment;
  }

  /**
   * get mock data
   * @param {*} url url
   * @return {object} data
   */
  function getMockData(url) {
    const mockData = configMap.mock.find((x) => x.url === url);

    return new Promise((resolve, reject) => resolve(mockData.data));
  }

  /**
   * get data
   * @param {*} url url
   * @return {object} response
   */
  function get(url) {
    if (stateMap.environment === 'development') {
      return getMockData(url);
    }

    return fetch(url);
  }

  return {
    init: privateInit,
    get,
  };
})();

Game.Model = (function () {
  const configMap = {};

  /**
   * initialize model
   */
  function privateInit() {
  }

  /**
   * get game state
   * @return {PromiseLike<void> | Promise<void> | *}
   * @private
   */
  function _getGameState() {
    return Game.Data.get(`https://localhost:44330/api/Spel/${window.token}`)
      .then((res) => res.json());
  }

  function _getOver() {
    return Game.Data.get(`https://localhost:44330/api/Spel/${window.token}/over`)
      .then((res) => res.json());
  }

  function _getWinning() {
    return Game.Data.get(`https://localhost:44330/api/Spel/${window.token}/winning`)
    .then((res) => res.json());
  }

  return {
    init: privateInit,
    getGameState: _getGameState,
    getWinning: _getWinning,
    getOver: _getOver,
  };
})();
